from game.score.game_score.game_score import Game_Score


class Score():
    def __init__(self):
        self.score_manage = Game_Score()


    def set_combo_score(self, combo_none = 0, combo_one = 100, combo_two = 300, combo_three = 700, combo_four = 1000):
        self.score_manage.combo_one = combo_one
        self.score_manage.combo_two = combo_two
        self.score_manage.combo_three = combo_three
        self.score_manage.combo_four = combo_four


    def get_combo_score(self, grid, locked_positions):
        return self.score_manage.combo_analyser(grid, locked_positions)


