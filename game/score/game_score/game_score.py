

class Game_Score():
    def __init__(self):
        self.combo_none = 0
        self.combo_one = 0
        self.combo_two = 0
        self.combo_three = 0
        self.combo_four = 0

        # clear a row when it is filled
    def clear_rows(self, grid, locked):
        # need to check if row is clear then shift every other row above down one
        increment = 0
        for i in range(len(grid) - 1, -1, -1):      # start checking the grid backwards
            grid_row = grid[i]                      # get the last row
            if (0, 0, 0) not in grid_row:           # if there are no empty spaces (i.e. black blocks)
                increment += 1
                # add positions to remove from locked
                index = i                           # row index will be constant
                for j in range(len(grid_row)):
                    try:
                        del locked[(j, i)]          # delete every locked element in the bottom row
                    except ValueError:
                        continue
        # shift every row one step down
        # delete filled bottom row
        # add another empty row on the top
        # move down one step
        if increment > 0:
            # sort the locked list according to y value in (x,y) and then reverse
            # reversed because otherwise the ones on the top will overwrite the lower ones
            for key in sorted(list(locked), key=lambda a: a[1])[::-1]:
                x, y = key
                if y < index:                       # if the y value is above the removed index
                    new_key = (x, y + increment)    # shift position to down
                    locked[new_key] = locked.pop(key)

        return increment


    def combo_score(self, combo):
        if combo == 0:
            return self.combo_none
        if combo == 1:
            return self.combo_one
        if combo == 2:
            return self.combo_two
        if combo == 3:
            return self.combo_three
        if combo == 4:
            return self.combo_four


    
    def combo_analyser(self, grid, locked):
        result = self.clear_rows(grid, locked)
        return self.combo_score(result)