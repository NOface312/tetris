import json
import random


class RobotGameInterface:

    def __init__(self, speed_control, label):
        self.speed_control = speed_control
        self.answer = True
        self.last_emotion_time = 0
        self.label_phrase = label


    def SpeedUp(self, robot):
        self.speed_control.speed_up()

    
    def Phrase_Choise(self, emotion):
        data = ""
        with open('assets/json/emotion_list/emotions_phrases_list.json') as json_file:
            phrases = json.load(json_file)
            for group in phrases["phrases"]:
                if group.get(emotion) != None:
                    data = random.choice(group[emotion])
                    data = data["phrase"]
        return data

    def EmotionGenerated(self, robot, emotion):
        info = self.speed_control.speed_info()
        self.last_emotion_time = info["level_time"] / 1000
        self.answer = False

        self.label_phrase.set_phrase_status_label(self.Phrase_Choise(emotion))


