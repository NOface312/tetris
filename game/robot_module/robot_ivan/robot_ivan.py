import random


class RobotEmotions:
    def __init__(self, groups):
        self.__groups = groups

        self.__emotions = []
        for group in groups:
            for emotion in group:
                self.__emotions.append(emotion)

    def GenerateEmotion(self):
        return random.choice(self.__emotions)

    def GetEmotionGroup(self, emotion):
        currentGroup = 0
        for group in self.__groups:
            if group.count(emotion) != 0:
                break
            currentGroup += 1
        return currentGroup

    
class RobotShapes:
    def __init__(self, groups):
        self.__groups = groups

    def AnswerGroup(self, group, currentGroup):
        if currentGroup != group:
            temp = self.__groups[group]
            self.__groups[group] = self.__groups[currentGroup]
            self.__groups[currentGroup] = temp

    def GetShapesGroup(self, group):
        return self.__groups[group]


class RobotBehaviour:
    def __init__(self, emotionsGroups, shapesGroups, robotGameInterface, maxShapesToGenerate = 10):
        self.__emotionsHandler = RobotEmotions(emotionsGroups)
        self.__shapesHandler = RobotShapes(shapesGroups)
        self.__game = robotGameInterface
        
        self.__isGroupFirstTime = {}
        i = 0
        for group in emotionsGroups:
            self.__isGroupFirstTime[i] = True
            i += 1

        self.__maxShapesToGenerate = maxShapesToGenerate
        self.__shapesGenerated = self.__maxShapesToGenerate

        self.needToAnswer = False

    def AnswerEmotion(self, emotion):
        if self.needToAnswer:
            group = self.__emotionsHandler.GetEmotionGroup(emotion)
            self.__shapesHandler.AnswerGroup(group, self.__currentGroup)
            self.needToAnswer = False

    def GenerateShape(self):
        if self.__shapesGenerated >= self.__maxShapesToGenerate:
            self.__shapesGenerated = 0

            emotion = self.__emotionsHandler.GenerateEmotion()
            self.__currentGroup = self.__emotionsHandler.GetEmotionGroup(emotion)
            self.__currentShapeGroup = self.__shapesHandler.GetShapesGroup(self.__currentGroup)
            if self.__isGroupFirstTime[self.__currentGroup] == True:
                self.__isGroupFirstTime[self.__currentGroup] = False
            else:
                self.__game.SpeedUp(self)

            self.needToAnswer = True
            self.__game.EmotionGenerated(self, emotion)

        self.__shapesGenerated += 1

        return random.choice(self.__currentShapeGroup)