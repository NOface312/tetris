import json


class Robot_Assets():
    def __init__(self):
        self.emotion_groups = []
        self.shapes_groups = []

        with open('assets/json/emotion_list/emotions_group_list.json') as json_file:
            self.emotion_groups = json.load(json_file)

        with open('assets/json/emotion_list/shapes_emotions_list.json') as json_file:
            self.shapes_groups = json.load(json_file)



    
    def get_emotion_groups(self):
        emotion_groups = self.emotion_groups["emotions"]
        data = []

        for group in emotion_groups:
            data_temp = []
            for em in group:
                data_temp.append(em["emotion"])
            data.append(data_temp)
            
        return data


    def get_shapes_groups(self):
        shapes_groups = self.shapes_groups["shapes"]
        data = []

        for group in shapes_groups:
            data_temp = []
            for shape in group:
                data_temp.append(shape["shape"])
            data.append(data_temp)
            
        return data
