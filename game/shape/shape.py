import random
from game.shape.shape_create.shape_create import Create_Shape


class Shape():
    def __init__(self):
        self.shape = Create_Shape()

    
    def get_random_shape(self):
        return self.shape.create_shape(random.randint(0, 6))


    def get_shape(self, shape_type):
        return self.shape.create_shape(shape_type)