class Piece(object):
    def __init__(self, x, y, shape, shape_colors, shapes):
        self.x = x
        self.y = y
        self.shape = shape
        self.color = shape_colors[shapes.index(shape)]  # choose color from the shape_color list
        self.rotation = 0 


class Create_Shape():
    def __init__(self):
        self.S = [
            ['.....',
            '.....',
            '..00.',
            '.00..',
            '.....'],
            ['.....',
            '..0..',
            '..00.',
            '...0.',
            '.....']]

        self.Z = [
            ['.....',
            '.....',
            '.00..',
            '..00.',
            '.....'],
            ['.....',
            '..0..',
            '.00..',
            '.0...',
            '.....']]

        self.I = [
            ['.....',
            '..0..',
            '..0..',
            '..0..',
            '..0..'],
            ['.....',
            '0000.',
            '.....',
            '.....',
            '.....']]

        self.O = [
            ['.....',
            '.....',
            '.00..',
            '.00..',
            '.....']]

        self.J = [
            ['.....',
            '.0...',
            '.000.',
            '.....',
            '.....'],
            ['.....',
            '..00.',
            '..0..',
            '..0..',
            '.....'],
            ['.....',
            '.....',
            '.000.',
            '...0.',
            '.....'],
            ['.....',
            '..0..',
            '..0..',
            '.00..',
            '.....']]

        self.L = [
            ['.....',
            '...0.',
            '.000.',
            '.....',
            '.....'],
            ['.....',
            '..0..',
            '..0..',
            '..00.',
            '.....'],
            ['.....',
            '.....',
            '.000.',
            '.0...',
            '.....'],
            ['.....',
            '.00..',
            '..0..',
            '..0..',
            '.....']]

        self.T = [
            ['.....',
            '..0..',
            '.000.',
            '.....',
            '.....'],
            ['.....',
            '..0..',
            '..00.',
            '..0..',
            '.....'],
            ['.....',
            '.....',
            '.000.',
            '..0..',
            '.....'],
            ['.....',
            '..0..',
            '.00..',
            '..0..',
            '.....']]

        # index represents the shape
        self.shapes = [self.S, self.Z, self.I, self.O, self.J, self.L, self.T]
        self.shape_colors = [
            (0, 255, 0), 
            (255, 0, 0), 
            (0, 255, 255), 
            (255, 255, 0), 
            (255, 165, 0), 
            (0, 0, 255), 
            (128, 0, 128)]
        
    def choice_shape(self, shape_type):
        return self.shapes[shape_type]


    def create_shape(self, shape_type):
        return Piece(5, 0, self.choice_shape(shape_type), self.shape_colors, self.shapes)