from game.game_status.timer_status_label.timer_status_label import Timer_Status_Label
from game.game_status.timer_label.timer_label import Timer_Label
from game.game_status.phrase_label.phrase_label import Phrase_Label
from game.game_status.phrase_status_label.phrase_status_label import Phrase_Status_Label


class Game_Status():


    def __init__(self, surface, assets):
        self.timer_status_label = Timer_Status_Label(surface, assets)
        self.timer_label = Timer_Label(surface, assets)
        self.phrase_label = Phrase_Label(surface, assets)
        self.phrase_status_label = Phrase_Status_Label(surface, assets)

    def set_phrase_label(self, text):
        self.phrase_label.set_text_label(text)


    def set_phrase_status_label(self, text):
        self.phrase_status_label.set_text_label(text)


    def set_timer_status_label(self, text):
        self.timer_status_label.set_text_label(text)


    def set_timer_label(self, text):
        self.timer_label.set_text_label(text)


    def draw_labels(self):
        self.timer_status_label.draw_label()
        self.phrase_label.draw_label()
        self.timer_label.draw_label()
        self.phrase_status_label.draw_label()