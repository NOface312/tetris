import pygame



class Phrase_Status_Label():
    def __init__(self, surface, assets):
        self.surface = surface
        self.assets = assets
        self.text = ""


    def set_text_label(self, text):
        self.text = text


    def draw_label(self):
        font = pygame.font.SysFont('arial', 26)

        phrase_status_label = font.render(self.text, 1, (255, 255, 255))

        self.surface.blit(phrase_status_label, (580, 235))