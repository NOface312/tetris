import pygame



class Timer_Status_Label():
    

    def __init__(self, surface, assets):
        self.surface = surface
        self.assets = assets
        self.text = ""


    def set_text_label(self, text):
        self.text = text


    def draw_label(self):
        font = pygame.font.SysFont('arial', 26)

        timer_status_label = font.render(self.text, 1, (255, 255, 255))

        self.surface.blit(timer_status_label, (580, 345))