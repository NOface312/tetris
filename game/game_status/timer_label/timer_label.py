import pygame



class Timer_Label():
    def __init__(self, surface, assets):
        self.surface = surface
        self.assets = assets
        self.text = ""


    def set_text_label(self, text):
        self.text = text


    def draw_label(self):
        font = pygame.font.SysFont('arial', 26)

        timer_label = font.render(self.text, 1, (255, 255, 255))

        self.surface.blit(timer_label, (580, 305))