import random
import pygame
from datetime import datetime, date, time

from constructors.message_box.confirm_windows import MessageBox
from constructors.assets.assets import Assets
from game.screen_draw.screen_draw import Screen_Draw
from game.shape.shape import Shape
from game.speed_control.speed_contol import Speed_Control
from game.score.score import Score
from game.game_status.game_status import Game_Status
from game.robot_module.robot import RobotGameInterface
from game.robot_module.robot_assets.robot_assets import Robot_Assets
from game.robot_module.robot_ivan.robot_ivan import RobotBehaviour


class Tetris_Game():
    
    def __init__(self, player_name, datebase):
        self.game_status = True
        self.database = datebase
        self.database.refresh()

        self.highscore = self.database.get_top_players()[0].score

        self.player_name = player_name
        self.split_all = 20
        self.col = 10  # 10 columns
        self.row = 20  # 20 rows
        self.s_width = 900 # window width
        self.s_height = 700 # window height
        self.play_width = 300  # play window width; 300/10 = 30 width per block
        self.play_height = 600  # play window height; 600/20 = 20 height per block
        self.block_size = 30  # size of block

        self.top_left_x = (self.s_width - self.play_width) - 600
        self.top_left_y = self.s_height - self.play_height

        self.run = True

        self.messagebox = MessageBox()
        self.assets = Assets()
        


    def create_grid(self, locked_pos={}):
        grid = [[(0, 0, 0) for x in range(self.col)] for y in range(self.row)]  # grid represented rgb tuples

        # locked_positions dictionary
        # (x,y):(r,g,b)
        for y in range(self.row):
            for x in range(self.col):
                if (x, y) in locked_pos:
                    color = locked_pos[
                        (x, y)]  # get the value color (r,g,b) from the locked_positions dictionary using key (x,y)
                    grid[y][x] = color  # set grid position to color

        return grid


    def convert_shape_format(self, piece):
        positions = []
        shape_format = piece.shape[piece.rotation % len(piece.shape)]  # get the desired rotated shape from piece

        '''
        e.g.
        ['.....',
            '.....',
            '..00.',
            '.00..',
            '.....']
        '''
        for i, line in enumerate(shape_format):  # i gives index; line gives string
            row = list(line)  # makes a list of char from string
            for j, column in enumerate(row):  # j gives index of char; column gives char
                if column == '0':
                    positions.append((piece.x + j, piece.y + i))

        for i, pos in enumerate(positions):
            positions[i] = (pos[0] - 2, pos[1] - 4)  # offset according to the input given with dot and zero

        return positions


    # checks if current position of piece in grid is valid
    def valid_space(self, piece, grid):
        # makes a 2D list of all the possible (x,y)
        accepted_pos = [[(x, y) for x in range(self.col) if grid[y][x] == (0, 0, 0)] for y in range(self.row)]
        # removes sub lists and puts (x,y) in one list; easier to search
        accepted_pos = [x for item in accepted_pos for x in item]

        formatted_shape = self.convert_shape_format(piece)

        for pos in formatted_shape:
            if pos not in accepted_pos:
                if pos[1] >= 0:
                    return False
        return True


    # check if piece is out of board
    def check_lost(self, positions):
        for pos in positions:
            x, y = pos
            if y < 1:
                return True
        return False

    
    def get_window_dictionary(self, grid, score, last_score, speed):
        window_dictionary = {
            'grid':grid,
            'score':score,
            'last_score':last_score, 
            'speed':speed,
            'top_left_x':self.top_left_x,
            'top_left_y':self.top_left_y,
            'play_width':self.play_width,
            'play_height':self.play_height,
            'player_name':self.player_name,
            'split_all':self.split_all,
            'block_size':self.block_size,
            'row':self.row,
            'col':self.col
        }
        return window_dictionary


    def get_shape_dictionary(self, next_piece):
        shape_dictionary = {
            'piece':next_piece,
            'top_left_x':self.top_left_x,
            'top_left_y':self.top_left_y,
            'play_width':self.play_width,
            'play_height':self.play_height,
            'player_name':self.player_name,
            'split_all':self.split_all,
            'block_size':self.block_size,
            'row':self.row,
            'col':self.col
        }
        return shape_dictionary

    
    def get_datetime(self):
        date = datetime.now()
        date_format = str(str(date.day) + "." + str(date.month) + "." + str(date.year))
        return date_format

    
    def game_status_control(self, status):
        if status == "restart":
            self.run = False
            self.database.addPlayer(self.player_name, self.score, self.get_datetime())

        if status == "back to menu":
            self.run = False
            self.game_status = False
            self.database.addPlayer(self.player_name, self.score, self.get_datetime())

        if status == "Anger":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Happy":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Surprise":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Neutral":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Sad":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Disgust":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True

        if status == "Fear":
            self.robot.AnswerEmotion(status)
            self.robot_module.answer = True
        
        if status == False:
            self.run = False
            self.game_status = False
            self.database.addPlayer(self.player_name, self.score, self.get_datetime())
        
    
    def start_game(self, window):
        while self.game_status:
            
            locked_positions = {}
            self.create_grid(locked_positions)

            self.run = True

            self.shape = Shape()

            self.speed_control = Speed_Control()
            self.speed_control.set_default_speed(0.35)

            self.score_control = Score()
            self.score_control.set_combo_score()

            self.label_status = Game_Status(window, self.assets)
            self.label_status.set_phrase_label("Robot pharses:")
            self.label_status.set_phrase_status_label("Wait 30s and one line clean")
            self.label_status.set_timer_label("Timer for answer:")
            self.label_status.set_timer_status_label("")

            
            change_piece = False
            current_piece = self.shape.get_random_shape()
            next_piece = self.shape.get_random_shape()

            self.score = 0
            highscore = self.highscore

            screen = Screen_Draw(window, self.assets)

            self.robot_module = RobotGameInterface(self.speed_control, self.label_status)

            robot_assets = Robot_Assets()

            self.robot = RobotBehaviour(robot_assets.get_emotion_groups(), robot_assets.get_shapes_groups(), self.robot_module, 10)

            robot_logic = False

            while self.run:
                # need to constantly make new grid as locked positions always change
                grid = self.create_grid(locked_positions)

                self.speed_control.tick()
                speed_dictionary = self.speed_control.speed_info()

                """if speed_dictionary["level_time"]/1000 > 5:
                    self.speed_control.set_level_time(0)
                    if speed_dictionary["fall_speed"] > 0.15:   # until fall speed is 0.15
                        self.speed_control.speed_up(0.005)"""

                
                if robot_logic:
                    if not self.robot_module.answer:
                        self.time = int(self.robot_module.last_emotion_time + 6 - speed_dictionary["level_time"]/1000)
                        self.label_status.set_timer_status_label(str(self.time))
                    else: 
                        self.label_status.set_timer_status_label(str("You answer!"))
                
                if not self.robot_module.answer and speed_dictionary["level_time"]/1000 >= self.robot_module.last_emotion_time + 5:
                    self.game_status_control(self.messagebox.you_lose())
                    self.run = False
                    

                if speed_dictionary["fall_time"] / 1000 > speed_dictionary["fall_speed"]:
                    self.speed_control.set_fall_time(0)
                    current_piece.y += 1
                    if not self.valid_space(current_piece, grid) and current_piece.y > 0:
                        current_piece.y -= 1
                        # since only checking for down - either reached bottom or hit another piece
                        # need to lock the piece position
                        # need to generate new piece
                        change_piece = True

                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.run = False
                        pygame.display.quit()
                        quit()

                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_LEFT:
                            current_piece.x -= 1  # move x position left
                            if not self.valid_space(current_piece, grid):
                                current_piece.x += 1

                        elif event.key == pygame.K_RIGHT:
                            current_piece.x += 1  # move x position right
                            if not self.valid_space(current_piece, grid):
                                current_piece.x -= 1

                        elif event.key == pygame.K_DOWN:
                            # move shape down
                            current_piece.y += 1
                            if not self.valid_space(current_piece, grid):
                                current_piece.y -= 1

                        elif event.key == pygame.K_UP:
                            # rotate shape
                            current_piece.rotation = current_piece.rotation + 1 % len(current_piece.shape)
                            if not self.valid_space(current_piece, grid):
                                current_piece.rotation = current_piece.rotation - 1 % len(current_piece.shape)

                        elif event.key == pygame.K_1:
                            self.game_status_control("Anger")

                        elif event.key == pygame.K_2:
                            self.game_status_control("Happy")

                        elif event.key == pygame.K_3:
                            self.game_status_control("Disgust")

                        elif event.key == pygame.K_4:
                            self.game_status_control("Sad")

                        elif event.key == pygame.K_5:
                            self.game_status_control("Fear")

                        elif event.key == pygame.K_6:
                            self.game_status_control("Surprise")

                        elif event.key == pygame.K_7:
                            self.game_status_control("Neutral")

                        

                piece_pos = self.convert_shape_format(current_piece)

                # draw the piece on the grid by giving color in the piece locations
                for i in range(len(piece_pos)):
                    x, y = piece_pos[i]
                    if y >= 0:
                        grid[y][x] = current_piece.color

                if change_piece:  # if the piece is locked
                    for pos in piece_pos:
                        p = (pos[0], pos[1])
                        locked_positions[p] = current_piece.color       # add the key and value in the dictionary
                    current_piece = next_piece

                    change_piece = False
                    score_prev = self.score
                    self.score += self.score_control.get_combo_score(grid, locked_positions)

                    if score_prev < self.score and speed_dictionary["level_time"]/1000 >= 30:
                        robot_logic = True

                    if robot_logic:
                        next_piece = self.shape.get_shape(self.robot.GenerateShape())
                    else:
                        next_piece = self.shape.get_random_shape()

                    #self.update_score(score)

                    """if last_score < score:
                        last_score = score"""

                window_dictionary = self.get_window_dictionary(grid, self.score, highscore, speed_dictionary["speed"])
                shape_dictionary = self.get_shape_dictionary(next_piece)

                status = screen.draw_screen(window_dictionary, shape_dictionary)
                self.label_status.draw_labels()

                self.game_status_control(status)

                pygame.display.update()

                if self.check_lost(locked_positions):
                    self.game_status_control(self.messagebox.you_lose())
                    self.run = False
                    
                   

            pygame.display.update()
            #pygame.time.delay(2000)  # wait for 2 seconds
        #pygame.quit()

