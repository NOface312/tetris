from game.speed_control.game_speed.game_speed import Speed


class Speed_Control():


    def __init__(self):
        self.game_speed = Speed()


    def speed_up(self, speed = 0.05):
        self.game_speed.increase_speed(speed)


    def set_default_speed(self, speed):
        self.game_speed.set_speed(speed)


    def tick(self):
        self.game_speed.clock_tick()


    def speed_info(self):
        return self.game_speed.get_speed_info()

    
    def set_fall_time(self, fall_time):
        self.game_speed.set_fall_time(fall_time)


    def set_level_time(self, level_time):
        self.game_speed.set_level_time(level_time)