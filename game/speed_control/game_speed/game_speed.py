import pygame


class Speed():


    def __init__(self):
        self.fall_time = 0
        self.fall_speed = 0
        self.level_time = 0
        self.speed = 0
        self.clock = pygame.time.Clock()


    def clock_tick(self):
        self.fall_time += self.clock.get_rawtime()
        self.level_time += self.clock.get_rawtime()
        self.clock.tick()

    
    def set_speed(self, speed):
        self.fall_speed = speed
        self.speed = int(speed * 1000)


    def increase_speed(self, speed):
        self.fall_speed -= speed
        self.speed += int(speed * 1000)


    def set_fall_time(self, fall_time):
        self.fall_time = fall_time


    def set_level_time(self, level_time):
        self.level_time = level_time

    def get_speed_info(self):
        info = {
            'fall_time': self.fall_time,
            'fall_speed': self.fall_speed,
            'speed': self.speed,
            'level_time': self.level_time
        }
        return info
