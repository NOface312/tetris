from game.screen_draw.game_buttons.game_buttons import Menu_Buttons
from game.screen_draw.draw_window.draw_window import Window
from game.screen_draw.draw_shape.draw_shape import Shape


class Screen_Draw():


    def __init__(self, surface, assets):
        self.menu_buttons = Menu_Buttons(surface, assets)
        self.menu_buttons.create_buttons()

        self.window = Window(surface, assets)
        self.shape = Shape(surface, assets)


    def draw_buttons(self):
        return self.menu_buttons.button_draw_control()


    def draw_shape(self, shape_dictionary):
        self.shape.draw_next_shape(shape_dictionary)


    def draw_window(self, window_dictionary):
        self.window.draw_window(window_dictionary)


    def draw_screen(self, window_dictionary, shape_dictionary):
        self.draw_window(window_dictionary)
        self.draw_shape(shape_dictionary)
        status = self.draw_buttons()

        return status