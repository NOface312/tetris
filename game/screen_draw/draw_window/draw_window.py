import pygame


class Window():

    
    def __init__(self, surface, assets):
        self.surface = surface
        self.assets = assets

    
    def draw_grid(self, surface, dictionary):
        r = g = b = 0
        grid_color = (r, g, b)

        for i in range(dictionary["row"]):
            # draw grey horizontal lines
            pygame.draw.line(surface, grid_color, (dictionary["top_left_x"], dictionary["top_left_y"] + i * dictionary["block_size"]),
                            (dictionary["top_left_x"] + dictionary["play_width"], dictionary["top_left_y"] + i * dictionary["block_size"]))
            for j in range(dictionary["col"]):
                # draw grey vertical lines
                pygame.draw.line(surface, grid_color, (dictionary["top_left_x"] + j * dictionary["block_size"], dictionary["top_left_y"]),
                                (dictionary["top_left_x"] + j * dictionary["block_size"], dictionary["top_left_y"] + dictionary["play_height"]))


    def draw_window(self, dictionary):

        self.surface.fill((0, 0, 0))  # fill the self.surface with black

        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath_mario, 65, bold=True)
        label = font.render('TETRIS', 1, (255, 255, 255))  # initialise 'Tetris' text with white

        self.surface.blit(label, ((dictionary["top_left_x"] + dictionary["play_width"] / 2) - (label.get_width() / 2), 30))  # put self.surface on the center of the window

        font_size = 28

        # player name
        font = pygame.font.Font(self.assets.fontpath, font_size)
        label = font.render(str(dictionary["player_name"]) , 1, (255, 255, 255))

        start_x = dictionary["top_left_x"] + dictionary["play_width"] + 50
        start_y = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 100)

        self.surface.blit(label, (start_x - dictionary["split_all"], start_y + 200))

        # current score
        label = font.render('SCORE   ' + str(dictionary["score"]) , 1, (255, 255, 255))

        start_x = dictionary["top_left_x"] + dictionary["play_width"] + 50
        start_y = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 100)

        self.surface.blit(label, (start_x - dictionary["split_all"], start_y + 250))

        # last score
        label_hi = font.render('HIGHSCORE   ' + str(dictionary["last_score"]), 1, (255, 255, 255))

        start_x_hi = dictionary["top_left_x"] + dictionary["play_width"] + 50
        start_y_hi = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 100)

        self.surface.blit(label_hi, (start_x_hi - dictionary["split_all"], start_y_hi + 300))

        # speed
        label_hi = font.render('Speed   ' + str(int(dictionary["speed"])), 1, (255, 255, 255))

        start_x_sp = dictionary["top_left_x"] + dictionary["play_width"] + 50
        start_y_sp = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 100)

        self.surface.blit(label_hi, (start_x_sp - dictionary["split_all"], start_y_sp + 350))

        # line

        start_x_line_begin = dictionary["top_left_x"] + dictionary["play_width"] + 258
        start_y_line_begin = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 400)

        start_x_line_end = dictionary["top_left_x"] + dictionary["play_width"] + 258
        start_y_line_end = dictionary["top_left_y"] + (dictionary["play_height"] / 2 + 450)

        pygame.draw.line(self.surface, (255, 255, 255), (start_x_line_begin, start_y_line_begin), (start_x_line_end , start_y_line_end), 4)

        # line 2

        start_x_line_begin = dictionary["top_left_x"] + dictionary["play_width"] + 258
        start_y_line_begin = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 110)

        start_x_line_end = dictionary["top_left_x"] + dictionary["play_width"] + 600
        start_y_line_end = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 110)

        pygame.draw.line(self.surface, (255, 255, 255), (start_x_line_begin, start_y_line_begin), (start_x_line_end , start_y_line_end), 4)
        # draw content of the dictionary["grid"]
        for i in range(dictionary["row"]):
            for j in range(dictionary["col"]):
                # pygame.draw.rect()
                # draw a rectangle shape
                # rect(Surface, color, Rect, width=0) -> Rect
                pygame.draw.rect(self.surface, dictionary["grid"][i][j],
                                (dictionary["top_left_x"] + j * dictionary["block_size"], dictionary["top_left_y"] + i * dictionary["block_size"], dictionary["block_size"], dictionary["block_size"]), 0)

        # draw vertical and horizontal dictionary["grid"] lines
        self.draw_grid(self.surface, dictionary)

        # draw rectangular border around play area
        border_color = (255, 255, 255)
        pygame.draw.rect(self.surface, border_color, (dictionary["top_left_x"], dictionary["top_left_y"], dictionary["play_width"], dictionary["play_height"]), 4)

        img = pygame.image.load(self.assets.robot)
        img.convert()
        img = pygame.transform.rotozoom(img, 0, 0.5)
        rect = img.get_rect()
        rect.center = 650, 90
        self.surface.blit(img, rect)

        """font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        label_hi = font.render("Robot Phrase Template", 1, (255, 255, 255))

        self.surface.blit(label_hi, (580, 230))

        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        label_hi = font.render("Status Phrase Template", 1, (255, 255, 255))

        self.surface.blit(label_hi, (580, 660))"""

        # pygame.display.update()