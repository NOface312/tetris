import pygame


class Shape():


    def __init__(self, surface, assets):
        self.surface = surface
        self.assets = assets


        # draws the upcoming dictionary["piece"]
    def draw_next_shape(self, dictionary):
        font = pygame.font.Font(self.assets.fontpath, 30)
        label = font.render('Next shape', 1, (255, 255, 255))

        start_x = dictionary["top_left_x"] + dictionary["play_width"] + 50
        start_y = dictionary["top_left_y"] + (dictionary["play_height"] / 2 - 100)

        shape_format = dictionary["piece"].shape[dictionary["piece"].rotation % len(dictionary["piece"].shape)]

        for i, line in enumerate(shape_format):
            row = list(line)
            for j, column in enumerate(row):
                if column == '0':
                    pygame.draw.rect(self.surface, dictionary["piece"].color, (start_x + j*dictionary["block_size"], start_y + i*dictionary["block_size"], dictionary["block_size"], dictionary["block_size"]), 0)

        self.surface.blit(label, (start_x - dictionary["split_all"] + 20, start_y - 30))

        border_color = (255, 255, 255)
        pygame.draw.rect(self.surface, border_color, (start_x - dictionary["split_all"], start_y - 40, dictionary["play_width"] - 100, (dictionary["play_height"] / 2 - 80)), 4)

        # pygame.display.update()