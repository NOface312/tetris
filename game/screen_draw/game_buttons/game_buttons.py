import pygame
from constructors.button.buttons import Button
from constructors.message_box.confirm_windows import MessageBox
from constructors.functions.functions import Return_Functions


class Menu_Buttons():


    def __init__(self, surface, assets):
        self.surface = surface
        self.messagebox = MessageBox()
        self.functions = Return_Functions()
        self.assets = assets


    # create buttons
    def create_buttons(self):
        
        # buttons
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 30, bold=True)

        self.restart = Button(330, 100, 200, 60, "Restart", self.surface, font, self.messagebox.restart_game)

        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 30, bold=True)

        self.home = Button(330, 170, 200, 60, "BACK  TO  HOME", self.surface, font, self.messagebox.go_home)

        #ROBOT BUTTONS
        split_x_1 = 240
        split_x_2 = 410
        spilt_y = 80
        button_all_y = 390
        #Anger
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.anger = Button(330 + split_x_1, button_all_y, 150, 60, "Anger", self.surface, font, self.functions.anger_button, color = (181, 13, 32), hover_col = (75, 225, 255), click_col = (232, 12, 37), text_col = (255, 255, 255))

        #Happy
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.happy = Button(330 + split_x_2, button_all_y, 150, 60, "Happy", self.surface, font, self.functions.happy_button, color = (196, 188, 20), hover_col = (75, 225, 255), click_col = (227, 218, 23), text_col = (255, 255, 255))
        #Disgust

        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.disgust = Button(330 + split_x_1, button_all_y + spilt_y, 150, 60, "Disgust", self.surface, font, self.functions.digust_button, color = (56, 54, 10), hover_col = (75, 225, 255), click_col = (41, 39, 7), text_col = (255, 255, 255))
        
        #Sad
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.sad = Button(330 + split_x_2, button_all_y + spilt_y, 150, 60, "Sad", self.surface, font, self.functions.sad_button, color = (25, 54, 71), hover_col = (75, 225, 255), click_col = (35, 79, 105), text_col = (255, 255, 255))

        #Fear
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.fear = Button(330 + split_x_1, button_all_y + spilt_y*2, 150, 60, "Fear", self.surface, font, self.functions.fear_button, color = (0, 0, 0), hover_col = (75, 225, 255), click_col = (20, 20, 20), text_col = (255, 255, 255))

        #Surprise
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.surprise = Button(330 + split_x_2, button_all_y + spilt_y*2, 150, 60, "Surprise", self.surface, font, self.functions.surprise_button, color = (75, 12, 82), hover_col = (75, 225, 255), click_col = (105, 16, 115), text_col = (255, 255, 255))

        #Neutral
        pygame.font.init()  # initialise font
        font = pygame.font.Font(self.assets.fontpath, 26, bold=True)

        self.neutral = Button(330 + split_x_1 + 90, button_all_y + spilt_y*3, 150, 60, "Neutral", self.surface, font, self.functions.neutral_button, color = (15, 138, 44), hover_col = (75, 225, 255), click_col = (9, 82, 26), text_col = (255, 255, 255))


    
    # draw button
    def button_draw_control(self):
        if self.restart.draw_button():
            if self.restart.button_push_event():
                return "restart"

        if self.home.draw_button():
            if self.home.button_push_event():
                return "back to menu"

        if self.anger.draw_button():
            if self.anger.button_push_event():
                return "Anger"

        if self.happy.draw_button():
            if self.happy.button_push_event():
                return "Happy"

        if self.disgust.draw_button():
            if self.disgust.button_push_event():
                return "Disgust"

        if self.sad.draw_button():
            if self.sad.button_push_event():
                return "Sad"

        if self.fear.draw_button():
            if self.fear.button_push_event():
                return "Fear"

        if self.surprise.draw_button():
            if self.surprise.button_push_event():
                return "Surprise"

        if self.neutral.draw_button():
            if self.neutral.button_push_event():
                return "Neutral"



