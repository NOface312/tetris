from tkinter import *
from tkinter import messagebox


class MessageBox():


    def __init__(self):
        pass


    #restart game
    def restart_game(self):
        Tk().wm_withdraw()
        MsgBox = messagebox.askquestion('Restart Application','Do you really want to restart over?', icon = 'warning')

        if MsgBox == "yes":
            return True
        else:
            return False


    # go home
    def go_home(self):
        Tk().wm_withdraw()
        MsgBox = messagebox.askquestion('Back to Menu Application','Do you really want to go to the menu?', icon = 'warning')

        if MsgBox == "yes":
            return True
        else:
            return False


    # lose game
    def you_lose(self):
        Tk().wm_withdraw()
        MsgBox = messagebox.askquestion('Game Over Application','Want to start over?', icon = 'warning')

        if MsgBox == "yes":
            return True
        else:
            return False