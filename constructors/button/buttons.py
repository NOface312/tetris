import pygame
from pygame.locals import *


class Button():

	def __init__(self, x, y, width, height, text, surface, font, event, color = (0, 65, 168), hover_col = (75, 225, 255), click_col = (50, 150, 255), text_col = (255, 255, 255)):
		self.x = x
		self.y = y
		self.width = width 
		self.height = height
		self.text = text
		self.surface = surface
		self.color = color
		self.clicked = False
		self.counter = 0
		self.text_col = text_col
		self.font = font
		self.hover_col = hover_col
		self.click_col = click_col
		self.event = event
		self.action = False

	def draw_button(self):
		self.action = False

		#get mouse position
		pos = pygame.mouse.get_pos()

		#create pygame Rect object for the button
		button_rect = Rect(self.x, self.y, self.width, self.height)
		
		#check mouseover and clicked conditions
		if button_rect.collidepoint(pos):
			if pygame.mouse.get_pressed()[0] == 1:
				self.clicked = True
				pygame.draw.rect(self.surface, self.click_col, button_rect)
			elif pygame.mouse.get_pressed()[0] == 0 and self.clicked == True:
				self.clicked = False
				self.action = True
			else:
				pygame.draw.rect(self.surface, self.color, button_rect)
		else:
			pygame.draw.rect(self.surface, self.color, button_rect)
		
		#add shading to button
		pygame.draw.line(self.surface, self.color, (self.x, self.y), (self.x + self.width, self.y), 2)
		pygame.draw.line(self.surface, self.color, (self.x, self.y), (self.x, self.y + self.height), 2)
		pygame.draw.line(self.surface, self.color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), 2)
		pygame.draw.line(self.surface, self.color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), 2)

		#add text to button
		text_img = self.font.render(self.text, True, self.text_col)
		text_len = text_img.get_width()
		
		pygame.draw.line(self.surface, (255, 255, 255), (self.x, self.y), (self.x + self.width, self.y), 2)
		pygame.draw.line(self.surface, (255, 255, 255), (self.x, self.y), (self.x, self.y + self.height), 2)
		pygame.draw.line(self.surface, (255, 255, 255), (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), 2)
		pygame.draw.line(self.surface, (255, 255, 255), (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), 2)


		self.surface.blit(text_img, (self.x + int(self.width / 2) - int(text_len / 2), self.y + 5))
		
		return self.action

	def button_push_event(self):
		return self.event()