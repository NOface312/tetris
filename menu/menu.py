import pygame
import pygame_menu
from functools import partial
from menu.main_menu.main_menu import Main_Menu

class Tetris_Menu():

    def __init__(self, game_resolution, menu_resolution):
        pygame.init()
        pygame.display.set_caption('Tetris')
        self.surface = pygame.display.set_mode((game_resolution['width'], game_resolution['height']))

        menu = Main_Menu(self.surface, game_resolution, menu_resolution)
        menu.start_menu()
        
