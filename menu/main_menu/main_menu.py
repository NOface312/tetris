import pygame
import pygame_menu
from functools import partial
from menu.main_menu.best_players.best_players import Best_Players
from menu.main_menu.instuctions.instructions import Instructions
from players_database.player_database import Players_Database
from game.tetris import Tetris_Game


class Main_Menu():
    def __init__(self, surface, game_resolution, menu_resolution):
        self.player_name = 'Player'
        self.surface = surface
        self.datebase = Players_Database("database.db")

        best_players_menu = Best_Players(game_resolution, menu_resolution, self.datebase)
        instructions_menu = Instructions(game_resolution, menu_resolution)

    
        self.menu = pygame_menu.Menu(
            height = menu_resolution['height'], 
            width = menu_resolution['width'],  
            title = 'Tetris', 
            theme = pygame_menu.themes.THEME_BLUE
        )
        self.menu.add.text_input(
            'Player Name:', 
            default=self.player_name,
            onchange=self.set_player_name
        )
        self.menu.add.button(
            'New game', 
            self.start_the_game
        )
        self.menu.add.button(
            'Best results', 
            best_players_menu.get_menu()
        )
        self.menu.add.button(
            'Instruction', 
            instructions_menu.get_menu()
        )
        self.menu.add.button(
            'Exit', 
            pygame_menu.events.EXIT
        )


    def paint_background(self, surface: 'pygame.Surface') -> None:
        surface.fill((12, 19, 36))


    def start_menu(self):
        self.menu.mainloop(
            surface = self.surface,
            bgfun=partial(self.paint_background, self.surface)
        )
    
    
    def set_player_name(self, value):
        self.player_name = value


    def start_the_game(self):
        s_width = 900
        s_height = 700
        win = pygame.display.set_mode((s_width, s_height))
        pygame.display.set_caption('Tetris')
        game = Tetris_Game(self.player_name, self.datebase)

        game.start_game(win)