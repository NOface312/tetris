import pygame
import pygame_menu
from functools import partial

class Best_Players():


    def __init__(self, game_resolution, menu_resolution, database):
        self.database = database
        players = self.database.get_top_players()

        self.theme = pygame_menu.themes.THEME_BLUE
        self.theme.widget_margin = (0, 0)

        self.best_players = pygame_menu.Menu(
            height = menu_resolution['height'], 
            width = menu_resolution['width'], 
            title = 'Best Players', 
            theme = pygame_menu.themes.THEME_BLUE
        )

        self.best_players.add.label("Leaderboard", align=pygame_menu.locals.ALIGN_CENTER, font_size=20)
        self.best_players.add.vertical_margin(30)

        self.table = self.best_players.add.table(table_id='my_self.table', font_size=20)
        self.table.default_cell_padding = 5
        self.table.default_row_background_color = 'white'
        self.table.add_row(['Players', 'Score', 'Data'],
                    cell_font=pygame_menu.font.FONT_OPEN_SANS_BOLD)
        
        for player in players:
            self.table.add_row([player.name, player.score, player.date])


        self.best_players.add.vertical_margin(30)
        self.best_players.add.button('Return to menu', pygame_menu.events.BACK)


    def get_menu(self):
        return self.best_players