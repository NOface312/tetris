from menu.menu import Tetris_Menu

def main():
    game_resolution = {
        'width': 900,
        'height': 700
    }
    menu_resolution = {
        'width':  700,
        'height': 700
    }
    menu = Tetris_Menu(game_resolution, menu_resolution)

if __name__ == "__main__":
    main()