import sqlite3


#класс-структура ячейки игрока с его рекордом, датой
class ScoreClass():
    

    def __init__(self, id_player, name, score, date):
        self.id = id_player
        self.name = name
        self.score = score
        self.date = date


#Класс базы данных
class DateBase():
    

    def __init__(self, name_file_db):
        self.file_name = name_file_db
        self.openFile()


    #метод для связи с бд, т.е. добавление игроков и создание бд
    def execute_query(self, query, data_tuple):
        self.cursor = self.connect.cursor()
        self.cursor.execute(query, data_tuple)
        self.connect.commit()


    #метод для чтения файла бд
    def execute_read_query(self, query):
        self.cursor = self.connect.cursor()
        result = None
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        return result
    

    #метод создания/открытия файла базы данных
    def openFile(self):
        self.connect = sqlite3.connect(self.file_name)
        create_players_table = """
        CREATE TABLE IF NOT EXISTS players (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT NOT NULL,
          score INTEGER,
          date TEXT
        );"""
        data_tuple = ()
        self.execute_query(create_players_table,data_tuple)


    #метод сортировки пользователей по количеству очков и возврата до 10 лучших игроков в формате list 
    def getTopTen(self):
        result: ScoreClass = []
        top_ten_players = " SELECT id, name, score, date FROM players ORDER BY score DESC LIMIT 10"
        players = self.execute_read_query(top_ten_players)
        for player in players:
            buff = ScoreClass(player[0], player[1], player[2], player[3])
            result.append(buff)
        return result
        

    #Метод добавления игрока(имя, счёт, дата+время)
    def addPlayer(self, player: ScoreClass):
        create_players = """
        INSERT INTO
          players (name, score, date)
        VALUES
          (?, ?, ?);
        """
        data_tuple = (player.name, player.score, player.date)
        self.execute_query(create_players, data_tuple)
        

    #Метод вывода всех элементов базы данных в формате list
    def getAllPlayers(self):
        result: ScoreClass = []
        select_players = "SELECT * from players"
        players = self.execute_read_query(select_players)
        for player in players:
            buff = ScoreClass(player[0] ,player[1], player[2], player[3])
            result.append(buff)
        return result




#ТЕСТЫ РАБОТЫ КЛАССА DateBase
"""file = DateBase('date.db')

test = ScoreClass(1, "clop", 1000, "11.11.2011")
print(test.name, test.score, test.date)
file.addPlayer(test)

top = file.getTopTen()
allPlayers = file.getAllPlayers()
print("TOP:")
for i in range(len(top)):
    print(top[i].id, top[i].name, top[i].score, top[i].date)
print("ALL:")
for i in range(len(allPlayers)):
    print(allPlayers[i].id, allPlayers[i].name, allPlayers[i].score, allPlayers[i].date)"""

"""file = DateBase('date.db')

for i in range(1, 10):
    test = ScoreClass(1, "clop", 1000, "11.11.2011")
    #print(test.name, test.score, test.date)
    file.addPlayer(test)
    print("sas")

allPlayers = file.getAllPlayers()
print(len(allPlayers))
for i in range(len(allPlayers)):
    print(allPlayers[i].id, allPlayers[i].name, allPlayers[i].score, allPlayers[i].date)"""