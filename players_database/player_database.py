from players_database.database.database import DateBase, ScoreClass
import json

class Players_Database():
    def __init__(self, path):
        self.path = path
        self.database = DateBase(self.path)

        if len(self.database.getAllPlayers()) < 10:
            with open('assets/json/default_records/default_records.json') as json_file:
                data = json.load(json_file)
                for player in data["players"]:
                    self.database.addPlayer(
                        ScoreClass(player["id"], player["name"], player["score"], player["date"]))


    def get_top_players(self):
        return self.database.getTopTen()


    def addPlayer(self, name, score, date):
        player = ScoreClass(1, name, score, date)
        self.database.addPlayer(player)

    
    def refresh(self):
        self.database = DateBase(self.path)

